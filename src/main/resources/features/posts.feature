Feature: Posts Page Tests

  Background:
    Given user is logged in to admin test-app page

  Scenario: Create new post draft
    Given I have opened dashboard page
    When I type title in quick draft section
    And I type content in quick draft section
    And I press save draft button
    And I have opened posts page
    Then I see my quick draft
    And I can move draft to trash