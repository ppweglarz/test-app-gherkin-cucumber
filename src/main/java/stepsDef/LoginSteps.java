package stepsDef;

import io.cucumber.java.en.Given;
import pages.LoginPage;
import pages.TopMenu;

public class LoginSteps {

    private LoginPage loginPage;
    private TopMenu topMenu;

    public LoginSteps() {
        loginPage = new LoginPage(Context.getDriver());
        topMenu = new TopMenu(Context.getDriver());
    }

    @Given("user is logged in to admin test-app page")
    public void loginToApplication() {
        if (!loginPage.isElementPresent(topMenu.getWelcomeMessage())) {
            loginPage.assertAdminLoginPage();
            loginPage.setUser(Context.getConfig().getApplicationUser());
            loginPage.setUserPassword(Context.getConfig().getApplicationPassword());
            loginPage.pressLoginButton();
        }
    }

}
