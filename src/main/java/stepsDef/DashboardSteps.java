package stepsDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import pages.DashboardPage;

public class DashboardSteps {

    private DashboardPage dashboardPage;

    public DashboardSteps() {
        dashboardPage = new DashboardPage(Context.getDriver());
    }

    @When("I type title in quick draft section")
    public void typeTitleQuickDraft() {
        dashboardPage.setTitleQuickDraft(Context.getRandomText());
    }

    @And("I type content in quick draft section")
    public void typeContentQuickDraft() {
        dashboardPage.setContentQuickDraft("This is my first post. Welcome!");
    }

    @And("I press save draft button")
    public void pressSaveDraftButton() {
        dashboardPage.pressSaveDraftButton();
    }

}
