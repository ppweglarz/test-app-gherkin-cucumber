package stepsDef;

import io.cucumber.java.en.Then;
import pages.TopMenu;

public class TopMenuSteps {

    private TopMenu topMenu;

    public TopMenuSteps() {
        topMenu = new TopMenu(Context.getDriver());
    }

    @Then("Welcome message is displayed for the user")
    public void assertTestAppAdminPanel() {
        topMenu.assertWelcomeUser(Context.getConfig().getApplicationUserName());
    }
}
