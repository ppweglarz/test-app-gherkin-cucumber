package stepsDef;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.LeftMenu;

public class LeftMenuSteps {

    LeftMenu leftMenu;
    public LeftMenuSteps() {
        leftMenu = new LeftMenu(Context.getDriver());
    }

    @Given("I have opened dashboard page")
    public void pressDashboardButton() {
        leftMenu.pressDashboardButton();
    }

    @Then("I have opened posts page")
    public void pressPostsButton() {
        leftMenu.pressPostsButton();
    }
}
