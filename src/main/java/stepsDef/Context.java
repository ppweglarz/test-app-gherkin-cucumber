package stepsDef;

import config.Config;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.UUID;


public class Context {
    @Getter
    private static final Config config = new Config();
    @Getter
    private static final String randomText = UUID.randomUUID().toString().substring(1, 15);
    @Getter
    private static WebDriver driver;

    @BeforeAll
    public static void before() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get(config.getApplicationUrl());
    }

    @AfterAll
    public static void after() {
        driver.quit();
    }

}
