package stepsDef;

import io.cucumber.java.en.And;
import pages.BulkActionsMenu;
import pages.PostsPage;

public class PostsSteps {

    private PostsPage postsPage;
    private BulkActionsMenu bulkActionsMenu;

    public PostsSteps() {
        postsPage = new PostsPage(Context.getDriver());
        bulkActionsMenu = new BulkActionsMenu(Context.getDriver());
    }

    @And("I see my quick draft")
    public void assertNewQuickDraft() {
        postsPage.assertNewPost(Context.getRandomText());
    }

    @And("I can move draft to trash")
    public void moveToTrash(){
        postsPage.markPost(Context.getRandomText());
        bulkActionsMenu.selectActions("Move to Trash").pressApplyButton();
    }

}

