package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeftMenu extends BasePage {

    public LeftMenu(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[contains(text(), 'Posts')]")
    private WebElement postsButton;

    @FindBy(xpath = "//div[contains(text(), 'Dashboard')]")
    private WebElement dashboardButton;

    public void pressPostsButton() {
        press(postsButton);
    }

    public void pressDashboardButton() {
        press(dashboardButton);
    }


}
