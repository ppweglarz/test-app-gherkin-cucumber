package pages;

import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class TopMenu extends BasePage {

    public TopMenu(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".display-name:first-child")
    private WebElement welcomeMessage;

    public void assertWelcomeUser(String userName) {
        Assert.assertEquals(welcomeMessage.getText(), userName);
    }

}
