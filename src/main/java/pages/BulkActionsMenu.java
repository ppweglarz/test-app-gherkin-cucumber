package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class BulkActionsMenu extends BasePage {

    public BulkActionsMenu(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#bulk-action-selector-top")
    private WebElement bulkActions;

    @FindBy(css = "#doaction")
    private WebElement applyButton;

    public BulkActionsMenu selectActions(String actionName) {
        new Select(bulkActions).selectByVisibleText(actionName);
        return this;
    }

    public BulkActionsMenu pressApplyButton() {
        press(applyButton);
        return this;
    }

}
