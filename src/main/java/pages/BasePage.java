package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    protected void press(WebElement element) {
        element.click();
    }

    protected void type(WebElement element, String value) {
        element.clear();
        element.sendKeys(value);
    }

    public boolean isElementPresent(WebElement element) {
        try {
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(0));
            element.isDisplayed();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            return true;
        } catch (NoSuchElementException exc) {
            return false;
        }
    }


}
