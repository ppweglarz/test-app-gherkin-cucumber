package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#user_login")
    private WebElement userLogin;

    @FindBy(css = "#user_pass")
    private WebElement userPassword;

    @FindBy(css = "#wp-submit")
    private WebElement loginButton;

    public void setUser(String user) {
        type(userLogin, user);
    }

    public void setUserPassword(String password) {
        type(userPassword, password);
    }

    public void assertAdminLoginPage() {
        Assert.assertTrue(loginButton.isDisplayed());
    }

    public void pressLoginButton() {
        press(loginButton);
    }
}
