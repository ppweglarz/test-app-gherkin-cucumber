package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage extends BasePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#title")
    private WebElement titleQuickDraft;

    @FindBy(css = "#content")
    private WebElement contentQuickDraft;

    @FindBy(css = "#save-post")
    private WebElement quickDraftButton;

    public void setTitleQuickDraft(String title) {
        type(titleQuickDraft, title);
    }

    public void setContentQuickDraft(String content){
        type(contentQuickDraft, content);
    }

    public void pressSaveDraftButton() {
        press(quickDraftButton);
    }
}

