package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PostsPage extends BasePage {

    public PostsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindAll({@FindBy(xpath = "//a[@class='row-title']")})
    private List<WebElement> postTitle;

    @FindAll({@FindBy(xpath = "//th[@class='check-column']/input")})
    private List<WebElement> postCheckBox;

    public void assertNewPost(String titlePost) {
        boolean isTitleMatched = false;
        for (WebElement element : postTitle) {
            if (element.getText().equals(titlePost)) {
                isTitleMatched = true;
                break;
            }
        }
        Assert.assertTrue(isTitleMatched);
    }

    public void markPost(String titlePost) {
        int index = 0;
        for (WebElement element : postTitle) {
            if (element.getText().equals(titlePost)) {
                press(postCheckBox.get(index));
                break;
            }
            index++;
        }
    }

}
